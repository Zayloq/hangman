# Created by Thien Thach Nguyen 18/8/2019
from random_word import RandomWords
word_generator = RandomWords()
keep_trying = True
correct_letters = 0
incorrect_letters = 0
lifes = 5
some_word = word_generator.get_random_word()
first_time = 1
letter_guessed = []

# Intro
print("Hi and welcome to my hangman game!\nI'll generate a random word for you to guess.\nNote: You have 5 lifes\n")

while keep_trying == True:
    continue_wish = input("Do you wish to continue?\n1: Yes\n2: No\n")
    if lifes == 0:
        print("You have 0 lifes left and have died! Better luck next time.")
        break

    if continue_wish == "No" or continue_wish == "no":
        print("\nAh you gave up! I'm sure you did well!")    
        break
    elif correct_letters == len(some_word):
        print(f"Congratulation! You've guessed the word {some_word} with {lifes} left. You can be proud of yourself.")
        break

    elif continue_wish == "Yes" or continue_wish == "yes":   
            print(f"\nThe word is {len(some_word)} characters long.")

            if first_time == 1:
               print("I can see this is your first try. I wish you the best of luck!") 
               first_time = 0
            else: 
                print(f"You've guessed the following letter(s) already: {letter_guessed}\nYou've also guessed {correct_letters} letters correctly and {incorrect_letters} letters incorrectly")
            guess = input("Guess a letter/word: ")
            if guess in letter_guessed:
                print("This letter has already been guessed, please try again.\n")
                continue
            elif guess == some_word:
                print("You guessed the word! Good job.\n")
                keep_trying = False
            elif guess in some_word:
                correct_letters += some_word.count(guess)
                print(f"Waow! Your guess for {guess} was one of the right letter!\n")
                letter_guessed.append(guess)
            else:
                lifes -= 1
                incorrect_letters += 1
                print(f"\nThe letter '{guess}' sadly wasn't one of the right letters :( Please try again\nYou've lost a life and now have {lifes} lifes left\n")
                letter_guessed.append(guess)
